const util = require('util');
const fs = require('fs');
const R = require('ramda');
const { toLines, convertLines } = require('./parser/parser');
const { wrapObject, header, footer } = require('./parser/templates');

const convert = (inputStr, wrapperObjName) => {
  const raw = R.split(/^\s*$/gm, inputStr);
  const items = R.map(toLines, raw);
  const nonEmpty = R.reject(R.isEmpty, items);
  const converted = R.map(convertLines, nonEmpty);
  const joined = converted.map(arr => arr.join('\n'));
  return wrapObject(wrapperObjName, joined.join('\n\n'));
};

const readF = util.promisify(fs.readFile);
const writeF = util.promisify(fs.writeFile);

(async () => { 
  const queryStr = await readF(__dirname + '/input/queries.graphql', 'utf-8');
  const mutationStr =  await readF(__dirname + '/input/mutations.graphql', 'utf-8');
  writeF(__dirname + '/output/converted.js', `${header}${convert(queryStr, 'queries')}${convert(mutationStr, 'mutations')}${footer}`);
})()
  .then(() => console.log('All done.'))
  .catch(console.log);
