
import {
  GraphQLEnumType,
  GraphQLInterfaceType,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
} from 'graphql';

// [ suppressed project specific exports from here ...]
import * as singleDeleteResultTypes from 'models/single-delete-result/graphql';

export default (someArgsHere) => {

  const queries = {
          allFunctionalOrganizationCharts: {
      type: functionalOrganizationChartTypes.functionalOrganizationChartConnectionType,
      description: ' Organograma Comercial'
      args: {
          limit: { type: GraphQLInt },
          skip: { type: GraphQLInt },
      },
      resolve: () => {   // ------------------  TODO: add a resolver
}
    },

    allFunctionalOrganizationChartInScope: {
      type: functionalOrganizationChartTypes.functionalOrganizationChartConnectionType,
      description: ' Organograma Comercial'
      args: {
          stamp: new GraphQLNonNull(stampInput),
      },
      resolve: () => {   // ------------------  TODO: add a resolver
}
    },

    functionalOrganizationChart: {
      type: functionalOrganizationChartTypes.functionalOrganizationChartType,  //  ---------- TODO:  Check if this auto-generated return type is correct,
      description: ' Organograma Comercial'
      args: {
          id:  { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: () => {   // ------------------  TODO: add a resolver
}
    },    
  }
  const mutations = {
          cloneFunctionalOrganizationCharts: {
      type: functionalOrganizationChartTypes.functionalOrganizationChartType,  //  ---------- TODO:  Check if this auto-generated return type is correct,
      description: ' Organograma Comercial'
      args: {
          stamp: new GraphQLNonNull(stampInput),
          sources: { type: new GraphQLNonNull(new GraphQLList(GraphQLString)) },
      },
      resolve: () => {   // ------------------  TODO: add a resolver
}
    },

    createFunctionalOrganizationChart: {
      type: functionalOrganizationChartTypes.functionalOrganizationChartType,  //  ---------- TODO:  Check if this auto-generated return type is correct,
      description: ' Organograma Comercial'
      args: {
          stamp: new GraphQLNonNull(stampInput),
          fields: new GraphQLNonNull(functionalOrganizationChartTypes.functionalOrganizationChartCreateFieldsInput),
      },
      resolve: () => {   // ------------------  TODO: add a resolver
}
    },

    patchFunctionalOrganizationChart: {
      type: functionalOrganizationChartTypes.functionalOrganizationChartType,  //  ---------- TODO:  Check if this auto-generated return type is correct,
      description: ' Organograma Comercial'
      args: {
          id:  { type: new GraphQLNonNull(GraphQLString) },
          fields: new GraphQLNonNull(functionalOrganizationChartTypes.functionalOrganizationChartPatchFieldsInput),
      },
      resolve: () => {   // ------------------  TODO: add a resolver
}
    },

    deleteFunctionalOrganizationChart: {
      type: singleDeleteResultTypes.singleDeleteArrayResultType,
      description: ' Organograma Comercial'
      args: {
          id:  { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: () => {   // ------------------  TODO: add a resolver
}
    },    
  }
  } 

  return {
    queries,
    mutations,
 };
}
