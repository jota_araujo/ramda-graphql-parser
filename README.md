I was asked to convert a bunch of files from [GraphqQL schema syntax](https://dev-blog.apollodata.com/webpacking-your-graphql-documents-bf9697ed259b) to [GraphQL.js](https://github.com/graphql/graphql-js) on a tight schedule, so I whipped up a basic converter between the two formats using [Ramda.js](http://ramdajs.com/) and a bit of regular expression matching.

Not generically applicable by any measure but it did what I needed in order to automate the conversion process for the task at hand.

![automate-all-the-things](https://bytebucket.org/jota_araujo/ramda-graphql-parser/raw/2c39a2c97d0d9f82415d02db1475c5d3fb1ea1f5/automate-all-the-things1.png)