const util = require('util');
const fs = require('fs');
const R = require('ramda');
const R_ = require('ramda-extension');

const serviceName = 'functionalOrganizationChart'; // TODO: should passed as input
const translatedName = 'Organograma Comercial'; // TODO: should passed as input

const typeMaps = [
  ['String', '{ type: GraphQLString }'],
  ['String!', ' { type: new GraphQLNonNull(GraphQLString) }'],
  ['[String]!', '{ type: new GraphQLNonNull(new GraphQLList(GraphQLString)) }'],
  ['Int', '{ type: GraphQLInt }'],
  ['Int!', ' { type: new GraphQLNonNull(GraphQLInt) }'],
  ['BigInt', '{ type: GraphQLInt }'],
  ['BigInt!', ' { type: new GraphQLNonNull(GraphQLInt) }'],
  ['Boolean', '{ type: GraphQLBoolean }'],
  ['Boolean!', ' { type: new GraphQLNonNull(GraphQLBoolean) }'],
  ['SingleDeleteArrayResult', 'singleDeleteResultTypes.singleDeleteArrayResultType'],
];

const emptyResolver = `resolve: () => {   // ------------------  TODO: add a resolver
}`;

const getResolver = () => {
  // NOTE: Removed this functionality for simplicity
  return emptyResolver;
};

const knownTypes = R.map(R.zipObj(['from', 'to']), typeMaps);
const findKnownType = str => R.filter(R.propEq('from', str), knownTypes);
const hasKnownType = str => Boolean(findKnownType(str).length);
const containsStr = R.curry((a, b) => R.contains(a, R.toLower(b)));
const hasBang = R.contains('!');
const isInput = str => R_.containsAny(['create', 'patch', 'input'], R.toLower(str));
const addType = str => `${str}Type`;
const indentLine = R.pipe(R.multiply(2), R.repeat(' '), R.join(''));
const getMethodName = lines => `${R.match(/\w+/, lines[0])[0]}: {`;
const filterFieldLines = lines => R.dropLast(1, R.tail(lines));

const convertField = (str, transformer, notNull) => R.pipe(
  R.replace('!', ''),
  R_.toLowerFirst,
  str => transformer(str),
  str => (notNull ? `new GraphQLNonNull(${str})` : R.identity(str)),
)(str);

const toInputName = (str) => {
  const containsInput = R.contains('input', R.toLower(str));
  return containsInput ? R.replace('InputFields', 'FieldsInput', str) : `${serviceName}Types.${R_.toLowerFirst(str)}Input`;
};

const captureGroup = (rgx, index, str) => {
  const match = rgx.exec(str);
  return match[index];
};

const getReturnType = (lines, methodName) => {
  const str = R.takeLast(1, lines)[0];
  const isArray = R.contains('[', str);
  const res = isArray ? captureGroup(/\[(.*?)\]/, 1, str) : R.match(/\w+/, str)[0];
  if (R.contains('delete', R.toLower(str))) return 'type: singleDeleteResultTypes.singleDeleteArrayResultType';
  if (R.contains('all', R.toLower(methodName))) return `type: ${serviceName}Types.${serviceName}ConnectionType`;
  if (R.toLower(serviceName) === methodName) return `type: ${serviceName}Types.${R_.toLowerFirst(res)}`;
  return `type: ${serviceName}Types.${R_.toLowerFirst(res)}Type,  //  ---------- TODO:  Check if this auto-generated return type is correct`;
};

const convertFieldLine = (input) => {
  const [fieldName, fieldType] = R.map(R.trim, R.split(':', input));
  return R.cond([
    [hasKnownType, str => `${fieldName}: ${findKnownType(str)[0].to},`],
    [containsStr('connection'), type => `${fieldName}: ${convertField(type, addType, hasBang(type))},`],
    [isInput, type => `${fieldName}: ${convertField(type, toInputName, hasBang(type))},`],
    [R.T, () => `${input}  // --------------------  FIXME: attribute template not found for this pattern`],
  ])(fieldType);
};

module.exports.convertLines = (lines) => {
  const indent = (nestingLevel = 0) => indentLine(nestingLevel + 2);
  const fields = filterFieldLines(lines);
  const convertedFields = R.map(convertFieldLine, fields);
  const indentedFields = R.map(str => `${indent(3)}${str}`, convertedFields);
  const methodName = getMethodName(lines);
  const str = R.flatten([
    `${indent()}${methodName}`,
    `${indent(1)}${getReturnType(lines, methodName)},`,
    `${indent(1)}description: ' ${translatedName}'`,
    `${indent(1)}args: {`,
    indentedFields,
    `${indent(1)}},`,
    `${indent(1)}${getResolver(methodName)}`,
    `${indent()}},`,
  ]);
  return str;
};

module.exports.toLines = R.pipe(
  R.split(/\n/g),
  R.map(R.trim),
  R.reject(R.isEmpty),
);

