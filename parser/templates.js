module.exports.header = `
import {
  GraphQLEnumType,
  GraphQLInterfaceType,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLString,
  GraphQLInt,
} from 'graphql';

// [ suppressed project specific exports from here ...]
import * as singleDeleteResultTypes from 'models/single-delete-result/graphql';

export default (someArgsHere) => {
`;

module.exports.wrapObject = (name, content) => `
  const ${name} = {
      ${content}    
  }`;

module.exports.footer = `
  } 

  return {
    queries,
    mutations,
 };
}
`;
